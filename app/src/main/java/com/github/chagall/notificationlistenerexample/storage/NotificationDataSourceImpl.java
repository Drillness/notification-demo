package com.github.chagall.notificationlistenerexample.storage;

import androidx.annotation.NonNull;

import com.github.chagall.notificationlistenerexample.model.Notification;
import com.github.chagall.notificationlistenerexample.storage.abs.NotificationDao;
import com.github.chagall.notificationlistenerexample.storage.abs.NotificationDataSource;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class NotificationDataSourceImpl implements NotificationDataSource {
    @NonNull
    private final NotificationDao dao;

    public NotificationDataSourceImpl(@NonNull NotificationDao dao) {
        this.dao = dao;
    }

    @Override
    public Single<List<Notification>> getNotifications() {
        return dao.getNotifications();
    }

    @Override
    public Completable addNotification(Notification notification) {
        return Completable.fromAction(() -> {
            dao.addNotification(notification);
        });
    }
}
