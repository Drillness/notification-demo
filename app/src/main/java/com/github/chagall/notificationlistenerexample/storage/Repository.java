package com.github.chagall.notificationlistenerexample.storage;

import androidx.annotation.NonNull;

import com.github.chagall.notificationlistenerexample.model.Notification;
import com.github.chagall.notificationlistenerexample.storage.abs.NotificationDataSource;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Repository implements NotificationDataSource {

    @NonNull
    private final NotificationDataSource notificationDataSource;

    public Repository(@NonNull NotificationDataSource notificationDataSource) {
        this.notificationDataSource = notificationDataSource;
    }

    @Override
    public Single<List<Notification>> getNotifications() {
        return notificationDataSource
                .getNotifications()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable addNotification(Notification notification) {
        return notificationDataSource
                .addNotification(notification)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
