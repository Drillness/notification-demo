package com.github.chagall.notificationlistenerexample.storage.abs;


import com.github.chagall.notificationlistenerexample.model.Notification;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface NotificationDataSource {

    Single<List<Notification>> getNotifications();

    Completable addNotification(Notification notification);

}
