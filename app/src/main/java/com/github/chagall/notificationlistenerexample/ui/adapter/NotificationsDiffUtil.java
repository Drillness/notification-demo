package com.github.chagall.notificationlistenerexample.ui.adapter;

import androidx.recyclerview.widget.DiffUtil;

import com.github.chagall.notificationlistenerexample.model.Notification;

import java.util.List;

public class NotificationsDiffUtil extends DiffUtil.Callback {
    private List<Notification> oldList;
    private List<Notification> newList;

    public NotificationsDiffUtil(List<Notification> oldList,
                                 List<Notification> newNotification) {
        this.oldList = oldList;
        this.newList = newNotification;
    }

    @Override
    public int getOldListSize() {
        return oldList == null ? 0 : oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList == null ? 0 : newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Notification oldItem = oldList.get(oldItemPosition);
        Notification newItem = newList.get(newItemPosition);

        return areItemsTheSame(oldItemPosition, newItemPosition) &&
                checkEquals(oldItem.getText(), newItem.getText()) &&
                checkEquals(oldItem.getChannelId(), newItem.getChannelId());
    }

    private boolean checkEquals(String s1, String s2) {
        if (s1 == null && s2 == null)
            return true;

        if (s1 != null) {
            return s1.equalsIgnoreCase(s2);
        }

        return false;
    }

}
