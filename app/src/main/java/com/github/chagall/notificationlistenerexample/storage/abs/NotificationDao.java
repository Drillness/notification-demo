package com.github.chagall.notificationlistenerexample.storage.abs;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.github.chagall.notificationlistenerexample.model.Notification;

import java.util.List;

import io.reactivex.Single;


@Dao
public interface NotificationDao {

    @Query("SELECT * FROM notification")
    Single<List<Notification>> getNotifications();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addNotification(Notification notification);

}
