package com.github.chagall.notificationlistenerexample.model;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "notification")
public class Notification {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @Nullable
    private String channelId;

    @Nullable
    private String text;

    public Notification(@Nullable String channelId, @Nullable String text) {
        this.channelId = channelId;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    @Nullable
    public String getChannelId() {
        return channelId;
    }

    @Nullable
    public String getText() {
        return text;
    }

}
