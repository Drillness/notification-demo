package com.github.chagall.notificationlistenerexample;

import android.app.Application;

import androidx.room.Room;

import com.github.chagall.notificationlistenerexample.storage.NotificationDataSourceImpl;
import com.github.chagall.notificationlistenerexample.storage.Repository;
import com.github.chagall.notificationlistenerexample.storage.abs.NotificationDatabase;

public class DemoApp extends Application {
    private static Repository repository;

    @Override
    public void onCreate() {
        super.onCreate();

        getRepo(this);
    }

    public static Repository getRepo(Application context) {
        if (repository == null)
            repository = new Repository(new NotificationDataSourceImpl(
                    database(context).booksListDao())
            );

        return repository;
    }

    static NotificationDatabase database(Application context) {
        return Room.databaseBuilder(context, NotificationDatabase.class, "notifications.db")
                .fallbackToDestructiveMigration()
                .build();
    }

}
