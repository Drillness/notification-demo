package com.github.chagall.notificationlistenerexample.service;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import androidx.annotation.RequiresApi;

import com.github.chagall.notificationlistenerexample.DemoApp;
import com.github.chagall.notificationlistenerexample.model.Notification;

/**
 * MIT License
 *
 *  Copyright (c) 2016 Fábio Alves Martins Pereira (Chagall)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class NotificationListenerExampleService extends NotificationListenerService {
    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        String channelId = sbn.getNotification().getChannelId();

        if (channelId.contains("SMS")) {
            StringBuilder builder = new StringBuilder();
            BundleUnwrapper uw = new BundleUnwrapper(sbn.getNotification().extras);
            builder.append("Title: ");
            builder.append(uw.getString(android.app.Notification.EXTRA_TITLE));
            builder.append("\n");
            builder.append("Message: ");
            builder.append(uw.getString(android.app.Notification.EXTRA_TEXT));

            DemoApp.getRepo(getApplication())
                    .addNotification(new Notification(channelId, builder.toString()))
                    .subscribe();
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
        // TODO Maybe handle as well?
    }

    private class BundleUnwrapper{
        private Bundle bundle;

        BundleUnwrapper(Bundle bundle) {
            this.bundle = bundle;
        }

        String getString(String key) {
            if (bundle == null)
                return "";

            return bundle.getString(key, "");
        }
    }

}