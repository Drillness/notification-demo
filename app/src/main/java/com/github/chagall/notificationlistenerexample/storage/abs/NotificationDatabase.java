package com.github.chagall.notificationlistenerexample.storage.abs;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.github.chagall.notificationlistenerexample.model.Notification;


@Database(
        entities = {Notification.class},
        version = 1,
        exportSchema = false)
public abstract class NotificationDatabase extends RoomDatabase {

    public abstract NotificationDao booksListDao();

}
