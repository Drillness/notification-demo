package com.github.chagall.notificationlistenerexample.ui.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.github.chagall.notificationlistenerexample.R;
import com.github.chagall.notificationlistenerexample.model.Notification;
import com.github.chagall.notificationlistenerexample.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    private MainActivity mainActivity;
    private List<Notification> nots = new ArrayList<>();

    public NotificationAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void updateList(List<Notification> data) {
        NotificationsDiffUtil callback = new NotificationsDiffUtil(this.nots, data);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(callback);

        this.nots.clear();
        this.nots.addAll(data);

        result.dispatchUpdatesTo(this);
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationViewHolder(mainActivity.getLayoutInflater().inflate(
                R.layout.item_message,
                null,
                false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.bind(nots.get(position));
    }

    @Override
    public int getItemCount() {
        return nots.size();
    }

    static class NotificationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView title;
        @BindView(R.id.tvText)
        TextView text;

        NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Notification notification) {
            title.setText(notification.getChannelId());
            text.setText(notification.getText());
        }
    }
}
